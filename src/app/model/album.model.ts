export interface Album {
    title: string,
    year: number,
    cover: string,
}