export interface Track {
    title: string,
    duration: number,
    rank: number
}