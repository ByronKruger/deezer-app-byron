import { Observable } from "rxjs";
import { Album } from "../model/album.model";
import { Artist } from "../model/artist.model";
import { Track } from "../model/track.model";

export interface IRepository { 
    getArtist(name: string): Observable<Artist[]>;
    getArtistAlbums(name: string): Observable<Album[]>;
    getArtistTracks(name: string): Observable<Track[]>;
}  