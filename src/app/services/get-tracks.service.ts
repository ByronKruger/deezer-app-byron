import { Injectable } from '@angular/core';
import { of, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { Track } from '../model/track.model';
import { DeezerRepositoryService } from './deezer-repository.service';

@Injectable({
  providedIn: 'root'
})
export class GetTracksService {
  tracksSubject = new Subject<Track[]>();

  getArtistTracks(name: string): void {
    this.repository.getArtistTracks(name)
    .subscribe((tracks: Track[]) => {
      tracks = tracks.sort((a, b) => (a.rank < b.rank) ? 1 : -1).slice(0, 5);
      this.tracksSubject.next(tracks);
    })
  }

  constructor(private repository: DeezerRepositoryService){}
}
