import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Album } from '../model/album.model';
import { Artist } from '../model/artist.model';
import { Track } from '../model/track.model';
import { IRepository } from './IRepository';

@Injectable({
  providedIn: 'root'
})
export class DeezerRepositoryService implements IRepository {
  baseUrl = "http://localhost:4200/api/";

  constructor(private http: HttpClient){}

  getArtist(name: string): Observable<Artist[]> {
    return this.http.get(this.baseUrl+"search/artist?q="+name).pipe(
      map((artists: any) => {
        return artists.data.map((artist: any)=>{
          return {
            id: artist.id,
            picture: artist.picture_big,
            fanCount: artist.nb_fan,
            albumCount: artist.nb_album,
            name: artist.name,
          }
        })
      })
    );
  }

  getArtistAlbums(name: string): Observable<Album[]> {
    return this.http.get(this.baseUrl+"search/album?q="+name).pipe(
      map((albums: any) => {
        return albums.data.map((album: any) => {
          return {
            title: album.title,
            year: 2008,
            cover: album.cover_xl
          }
        })
      })
    );
  }
  
  getArtistTracks(name: string): Observable<Track[]> {
      return this.http.get(this.baseUrl+"search/track?q="+name).pipe(
      map((tracks: any) => {
        return tracks.data.map((track: any) => {
          return {
            title: track.title,
            duration: track.duration,
            rank: track.rank,
          }
        })
      })
    );
  }

  getArtistById(id: string): Observable<Artist[]> {
    return this.http.get(this.baseUrl+"search/artist?q="+id).pipe(
      // return this.http.get(this.baseUrl+"artist/"+id).pipe(
      map((artists: any) => {
        return artists.data.map((artist: any) => {
          return {
            id: artist.id,
            picture: artist.picture_big,
            fanCount: artist.nb_fan,
            name: artist.name,
            albumCount: artist.nb_album
          }
        })
      })
    )
  } 
}
