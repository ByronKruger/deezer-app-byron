import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { Album } from '../model/album.model';
import { DeezerRepositoryService } from './deezer-repository.service';

@Injectable({
  providedIn: 'root'
})
export class GetAlbumService {
  albumsSubject = new Subject<Album[]>();

  getArtistAlbums(name: string): void {
    this.repository.getArtistAlbums(name)
    .subscribe((tracks: Album[]) => {
      this.albumsSubject.next(tracks);
    })
  }

  constructor(private repository: DeezerRepositoryService){}
}
