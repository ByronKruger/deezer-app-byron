import { newArray } from '@angular/compiler/src/util';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { Artist } from '../model/artist.model';
import { DeezerRepositoryService } from './deezer-repository.service';

@Injectable({
  providedIn: 'root'
})
export class ArtistSearchService {
  _name?: string;
  _artists?: Artist[];
  artistsSubject = new Subject<Artist[]>();
  artistSubject = new Subject<Artist>();

  constructor(private repository: DeezerRepositoryService){}

  public set name(name: string){
    this._name = name;
  }

  public get artists() {
    return this._artists;
  }

  public getArtist(name: string): void  {
    this.repository.getArtist(name)
    .subscribe((artistsArray) => {
      // this._artists = [...artistsArray];
      this.artistsSubject.next(artistsArray);
    })
  }

  public getArtistById(name: string): void  {
    this.repository.getArtistById(name)
    .subscribe((artist) => {
      this.artistSubject.next(artist[0]);
    })
  }
}
